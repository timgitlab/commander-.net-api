using Commander.Models;
using Microsoft.EntityFrameworkCore;

namespace Commander.Data
{
    public class CommanderContext : DbContext  //this comes from entityframework
    {
        public CommanderContext(DbContextOptions<CommanderContext> opt) : base(opt)  //base calls the constructor?
        {

        }
        public DbSet<Command> Commands { get; set; }  
        //this maps the Model to the dbcontext. if it is not sqlserver then there would be:
        // 1. different package reference
        // 2. different connection string
        // 3. different reference in the startup class
    }
}