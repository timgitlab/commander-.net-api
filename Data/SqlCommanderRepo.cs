using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Windows.Input;
using Commander.Models;
using Commander.Validation;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore.ChangeTracking;

namespace Commander.Data
{
    public class SqlCommanderRepo : ICommanderRepo
    {
        private readonly CommanderContext _context;
        private readonly ICommandsValidator _validator;

        public SqlCommanderRepo(CommanderContext context, ICommandsValidator validator)
        {
            _context = context;
            _validator = validator;
        }

        public Command CreateCommand(Command cmd)
        {
            if(cmd == null)
            {
                throw new ArgumentNullException(nameof(cmd));
            }
            if(_validator.CheckForDuplicate(cmd))
            {
                throw new DuplicateNameException();
            }
            _context.Commands.Add(cmd);
            //return _context.Commands.OrderBy(p => p.Id).Last().Id + 1;  //+1 since the changes have not been saved yet
            return cmd;
        }

        public void DeleteCommand(Command cmd)
        {
            if(cmd == null)
            {
                throw new ArgumentNullException("Duplicate Entry.");
            }
            _context.Commands.Remove(cmd);
        }

        public IEnumerable<Command> GetAllCommands()
        {
            return _context.Commands.ToList();
        }

        public List<string> GetAllPlatforms()
        {
            //IEnumerable<Command> uniqueCommandPlatforms = _context.Commands.DistinctBy(p => p.Platform);  -->Why does this not work?
            // List<string> uniquePlatforms = uniqueCommandPlatforms.Select(c => c.Platform).ToList();
            var uniquePlatforms = _context.Commands.Select(c => c.Platform).Distinct().ToList();
            return uniquePlatforms;
        }

        public Command GetCommandById(int id)
        {
            return _context.Commands.FirstOrDefault(p => p.Id == id);
        }

        public IEnumerable<Command> GetCommandsByPlatform(string platform)
        {
            return _context.Commands.Where(p => p.Platform == platform);
        }

        public bool SaveChanges()
        {
            return (_context.SaveChanges() >= 0);
        }

        public void UpdateCommand(Command cmd)
        {
            //Nothing
            //the dbcontext does it
        }
    }
}