using System.Collections;
using System.Collections.Generic;
using Commander.Models;

namespace Commander.Data
{
    public interface ICommanderRepo
    {
        bool SaveChanges();
        IEnumerable<Command> GetAllCommands();
        IEnumerable<Command> GetCommandsByPlatform(string platform);
        List<string> GetAllPlatforms();
        Command GetCommandById(int id);
        Command CreateCommand(Command cmd);
        void UpdateCommand(Command cmd);
        void DeleteCommand(Command cmd);
    }
}