using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection.Metadata.Ecma335;
using AutoMapper;
using Commander.Data;
using Commander.Dtos;
using Commander.Models;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.WebUtilities;

namespace Commander.Controllers
{
    [ApiController]
    public class CommandsController : ControllerBase
    {
        private readonly ICommanderRepo _repository;  //underscore denotes private readonly fields
        private readonly IMapper _mapper;

        public CommandsController(ICommanderRepo repository, IMapper mapper)  //constructor for this class 'ctor' shortcut. Takes MockCommanderRepo as an argument from startup.cs
        {
            _repository = repository;  //hand off the injected value to the private field
            _mapper = mapper;
        }

        //GET commands
        [HttpGet("/commands")]
        public ActionResult <IEnumerable<CommandReadDto>> GetAllCommands()
        {
            var commandItems = _repository.GetAllCommands();
            return Ok(_mapper.Map<IEnumerable<CommandReadDto>>(commandItems));
        }

        //GET commands/{id}
        [HttpGet("/commands/{id}", Name="GetCommandById")]
        public ActionResult <CommandReadDto> GetCommandById(int id)  //this name does not need to match the repository method name
        {
            var commandItem = _repository.GetCommandById(id);
            if(commandItem != null)
            {
                return Ok(_mapper.Map<CommandReadDto>(commandItem));
            }
            return NotFound();
        }

        //POST commands
        [HttpPost("/commands")]
        public ActionResult <CommandReadDto> CreateCommand(CommandCreateDto commandCreateDto)
        {
            var commandModel = _mapper.Map<Command>(commandCreateDto);
            commandModel = _repository.CreateCommand(commandModel);
            _repository.SaveChanges();

            var commandReadDto = _mapper.Map<CommandReadDto>(commandModel);
            return CreatedAtRoute(nameof(GetCommandById), new {id = commandModel.Id}, commandReadDto);  // routeName (method) , an object of the unique data for the method route , the content to return with the response
            //return CreatedAtRoute(nameof(GetAllCommands), new {id = 7}, commandReadDto); // this fails because dotnet checks the route is valid first, GetAllCommands/7 is not valid
        }

        //PUT commands/{id}
        [HttpPut("/commands/{id}")]
        public ActionResult UpdateCommand(int id, CommandUpdateDto commandUpdateDto)
        {
            var commandModelFromRepo = _repository.GetCommandById(id);
            if(commandModelFromRepo == null)
            {
                return NotFound();
            }
            _mapper.Map(commandUpdateDto, commandModelFromRepo);  //this goes from updateDto to the command retrieved, and updates the commandModelFromRepo
            //this also is monitored by the dbcontext, which is why there is no need to implement in sqlcommanderrepo
            _repository.UpdateCommand(commandModelFromRepo);
            _repository.SaveChanges();  //this saves the changes the dbcontext monitored above.
            return NoContent();
        }

        //PATCH commands/{id}
        [HttpPatch("/commands/{id}")]
        public ActionResult PartialUpdateCommand(int id, JsonPatchDocument<CommandUpdateDto> patchDoc)
        {
            var commandModelFromRepo = _repository.GetCommandById(id);
            if(commandModelFromRepo == null)
            {
                return NotFound();
            }
            var commandToPatch = _mapper.Map<CommandUpdateDto>(commandModelFromRepo);
            patchDoc.ApplyTo(commandToPatch, ModelState);
            if(!TryValidateModel(commandToPatch))
            {
                return ValidationProblem(ModelState);
            }
            _mapper.Map(commandToPatch, commandModelFromRepo);
            _repository.UpdateCommand(commandModelFromRepo);
            _repository.SaveChanges();
            return NoContent();
        }

        //DELETE commands/{id}
        [HttpDelete("/commands/{id}")]
        public ActionResult DeleteCommand(int id)
        {
            var commandModelFromRepo = _repository.GetCommandById(id);
            if(commandModelFromRepo == null)
            {
                return NotFound();
            }
            _repository.DeleteCommand(commandModelFromRepo);
            _repository.SaveChanges();
            return NoContent();
        }

        //GET platform/
        [HttpGet("/platform")]
        public ActionResult <List<string>> GetAllPlatforms()
        {
            List<string> platformItems = _repository.GetAllPlatforms();
            if(platformItems.Count() == 0)
            {
                return NotFound();
            }
            return Ok(platformItems);
        }
        
        //GET platform/{platform}
        [HttpGet("/platform/{platform}")]
        public ActionResult <IEnumerable<CommandReadDto>> GetCommandsByPlatform(string platform)
        {
            IEnumerable<Command> commandItems = _repository.GetCommandsByPlatform(platform);
            if(commandItems.Count() == 0)
            {
                return NotFound();
            }
            return Ok(_mapper.Map<IEnumerable<CommandReadDto>>(commandItems));
        }
    }
}