using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using Commander.Data;
using Commander.Models;
using Microsoft.Extensions.Hosting;

namespace Commander.Validation;

public class FirstCommandsValidator : ICommandsValidator
{
    private readonly CommanderContext _context;
    private readonly Command defaultCommand = new Command{Id=0, HowTo="Default", Line="Default", Platform="Default"};
    public FirstCommandsValidator(CommanderContext context)
    {
        _context = context;
    }
    public bool CheckForDuplicate(Command commandToAdd)
    {
        IEnumerable<Command> commandList = _context.Commands.ToList();
        Command firstMatch = commandList.FirstOrDefault(c => c.Line == commandToAdd.Line, defaultCommand);
        return firstMatch.Id != 0;
    }
}