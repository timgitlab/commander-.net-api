using Commander.Models;

namespace Commander.Validation
{
    public interface ICommandsValidator
    {
        bool CheckForDuplicate(Command command);
    }
}