This is a REST (ish) web api to manage a list of commands for different programs and tasks.

It currently has:
- read all entries, GET.
- read single entry, GET.
- read entries per platform, GET.
- read all platforms in db, GET.
- create entry, POST.
- overwrite entry, PUT.
- modify entry, PATCH.
- delete entry, DELETE.

**SQL Server**

Currently the database is set up on my server, in a docker container.
```
docker run -e "ACCEPT_EULA=Y" -e "SA_PASSWORD=[some_password]" -p 1433:1433 --name sql1 -h sql1 \ 
-d mcr.microsoft.com/mssql/server
```

- the SQL Server container version may have changed, check [here](https://hub.docker.com/_/microsoft-mssql-server").
- check the password rules for the SA_PASSWORD otherwise the container will start and stop immediately.

**API Running locally**

Need to set the environment variable for the database connection.
```
$env:DBPASSWORD = "[password]"
```

**API Container**
```
docker build -t commander .
docker run -p 8080:80 -e "DBPassword=[password]" commander
```
where commander is the optional name of the image built, and DBPassword is the password for the SQL Server.