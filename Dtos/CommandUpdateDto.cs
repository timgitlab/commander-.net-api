using System.ComponentModel.DataAnnotations;

namespace Commander.Dtos
{
    public class CommandUpdateDto : CommandCreateDto
    {
        //Nothing changes at this stage
    }
}