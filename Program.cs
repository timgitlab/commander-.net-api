using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Commander;
using Commander.Data;
using Commander.Validation;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json.Serialization;

var builder = WebApplication.CreateBuilder(args);

builder.Services.AddControllers();
builder.Configuration.AddEnvironmentVariables(prefix: "DBPassword");
builder.Services.AddHsts(options =>
{
    options.Preload = true;
    options.IncludeSubDomains = true;
});
builder.Services.AddHttpsRedirection(options =>
{
    options.RedirectStatusCode = (int)HttpStatusCode.TemporaryRedirect;
    options.HttpsPort = 443;
});

var startup = new Startup(builder.Configuration);
startup.ConfigureServices(builder.Services);

var app = builder.Build();
startup.Configure(app, app.Environment);
app.Run();

public class Startup
    {
        public Startup(IConfiguration configuration)  //this is a dotnet API
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddScoped<ICommanderRepo, SqlCommanderRepo>();  //dependency injection to implement interface. AddScoped is new per request.
            services.AddScoped<ICommandsValidator, FirstCommandsValidator>();
            services.AddDbContext<CommanderContext>(opt => opt.UseSqlServer  //lambda expression
                //(Configuration.GetConnectionString("CommanderConnection")));  //this is why we use connectionstring in appsettings
                (GetDBString()));
            services.AddControllers().AddNewtonsoftJson(s => {
                s.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
            });
            services.AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies());
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                Console.Write("This is development.\n");
                app.UseDeveloperExceptionPage();
            }
            else
            {
                Console.Write("This is production.\n");
                //app.UseHsts();  //middleware to to send 'http strict transport security protocol' headers to clients
            }

            //app.UseHttpsRedirection();  //middleware to redirect http requests to https
            app.UseRouting();
            app.UseAuthorization();
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }

        public string GetDBString()
        {
            string password = Configuration["DBPassword"];
            password = "Server=192.168.1.13;Initial Catalog=CommanderDB;User ID=CommanderAPI;Password=" + password + ";";
            return password;
        }
    }