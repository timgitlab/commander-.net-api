using System.ComponentModel.DataAnnotations;

namespace Commander.Models
{
    public class Command
    {
        //conventions say that this is required and not nullable. but key also works
        [Key]
        public int Id { get; set; }
        [Required]
        [MaxLength(250)]
        public string HowTo { get; set; }
        [Required]
        public string Line { get; set; }
        [Required]
        public string Platform { get; set; }
    }
}